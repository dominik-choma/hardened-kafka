import json


def append_line(file,line):
    file.write(line+'\n')

f = open("inventory", "w+")


zkvars='[zk] \n'
kbvars='[kb] \n'
zkglobalvars = ''
kbglobalvars = ''
zki=1
kbi=1
with open('../terraform/terraform.tfstate') as json_file:
    data = json.load(json_file)
    for resource in data['resources']:
        if resource['type'] == 'aws_spot_instance_request' and resource['name'].startswith("zk"):
            for instance in resource['instances']:
                zkvars+=instance['attributes']['public_ip']+" zookeeperid=%s"%zki +' publicDNS='+instance['attributes']['public_dns']+ '\n'
                zkglobalvars+="zk%s="%zki+instance['attributes']['private_ip']+'\n'
                zki=zki+1
        if resource['type'] == 'aws_spot_instance_request' and resource['name'].startswith("kb"):
            for instance in resource['instances']:
                kbvars+=instance['attributes']['public_ip']+" kafkaid=%s"%kbi +' publicDNS='+instance['attributes']['public_dns']+ '\n'
                kbglobalvars+="kb%s="%kbi+instance['attributes']['public_dns']+'\n'
                kbi=kbi+1

append_line(f,zkvars)
append_line(f,kbvars)
append_line(f,"[zk:vars]")
append_line(f,"ansible_ssh_user=ec2-user")
append_line(f,"ansible_ssh_private_key_file=/home/dominik/bigdata.pem")
append_line(f,zkglobalvars)

append_line(f,"[kb:vars]")
append_line(f,"ansible_ssh_user=ec2-user")
append_line(f,"ansible_ssh_private_key_file=/home/dominik/bigdata.pem")
append_line(f,kbglobalvars)

f.close()
json_file.close()

