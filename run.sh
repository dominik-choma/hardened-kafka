#!/usr/bin/env bash
cd terraform
terraform apply
cd ../ansible
python create_inventory.py
ansible-playbook -i inventory zoookeper.yaml
