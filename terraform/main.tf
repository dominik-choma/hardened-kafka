micro_price = "0.0035"
mini_price = "0.0035"
kafka_zookeeper_id = "sg-0ff5a49d792e3dfef"
kafka_broker_id = "sg-0ff5a49d792e3dfef"
ami_id = "ami-0862aabda3fb488b5"



provider "aws" {
  region                  = "eu-west-1"
  shared_credentials_file = "/home/dominik/.aws/credentials"
  profile                 = "default"
}




data "aws_security_group" "kafka-zookeeper" {
  id = kafka_zookeeper_id
}

data "aws_security_group" "kafka-broker" {
  id = kafka_broker_id
}


resource "aws_spot_instance_request" "zk1" {
  ami = ami_id
  instance_type = "t3.micro"
  spot_price    = micro_price
  spot_type     = "one-time"
  wait_for_fulfillment = true
  security_groups =[ "kafka-zookeeper"]
  key_name = "bigdata"
  tags = {
    Name = "zookeeper1"
  } 
}

resource "aws_spot_instance_request" "zk2" {
  ami = ami_id
  instance_type = "t3.micro"
  spot_price    = micro_price
  spot_type     = "one-time"
  wait_for_fulfillment = true
  security_groups =[ "kafka-zookeeper"]
  key_name = "bigdata"
  tags = {
    Name = "zookeeper2"
  }
}

resource "aws_spot_instance_request" "zk3" {
  ami = ami_id
  instance_type = "t3.micro"
  spot_price    = micro_price
  spot_type     = "one-time"
  wait_for_fulfillment = true
  security_groups =[ "kafka-zookeeper"]
  key_name = "bigdata"
  tags = {
    Name = "zookeeper3"
  }
}


resource "aws_spot_instance_request" "kb1" {
  ami = ami_id
  instance_type = "t3.mini"
  spot_price    = mini_price
  spot_type     = "one-time"
  wait_for_fulfillment = true
  security_groups =[ "kafka-broker"]
  key_name = "bigdata"
  tags = {
    Name = "kafka1"
  } 
}

resource "aws_spot_instance_request" "kb2" {
  ami = ami_id
  instance_type = "t3.mini"
  spot_price    = mini_price
  spot_type     = "one-time"
  wait_for_fulfillment = true
  security_groups =[ "kafka-broker"]
  key_name = "bigdata"
  tags = {
    Name = "kafka2"
  }
}

resource "aws_spot_instance_request" "kb3" {
  ami = ami_id
  instance_type = "t3.mini"
  spot_price    = mini_price
  spot_type     = "one-time"
  wait_for_fulfillment = true
  security_groups =[ "kafka-broker"]
  key_name = "bigdata"
  tags = {
    Name = "kafka3"
  }
}





